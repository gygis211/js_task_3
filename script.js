
let sidebarId = document.querySelector('#sidebarId'), // Нашел тег в котором буду менять класс
    mainId = document.querySelector('#mainId'), // Нашел тег в котором буду менять класс
    btnTheme = document.querySelector('#themeButton'); //присвоил переменной кнопку "Сменить тему" через ID

    // btnEdit = document.querySelector('.button_2'), // присвоил переменной кнопку "Редактировать" через класс
    // editTextBox = document.querySelector('.editTextBox');

btnTheme.onclick = () => { // функция будет менять класс элементов кода HTML при нажатии на элемент когда заданный в эту переменную
    sidebarId.classList.toggle('sidebar-dark'); // будет менять класс на указанный и обратно
    mainId.classList.toggle('main-dark'); // будет менять класс на указанный и обратно
};

// let flag = true; // переменная будет менять свое значение при работе функции ниже
// btnEdit.onclick = () => { // функция будет срабатывать на нажатие элемента кода присвоенного переменной
//     if (flag) {
//         flag = false;
//         editTextBox.contentEditable = "true"; // разрешает редактировать текст
//         btnEdit.textContent = "Сохранить"
//     } else {
//         flag = true;
//         editTextBox.contentEditable = "false"; // не разрешает редактировать текст
//         btnEdit.textContent = "Редактировать"
//     }
//     editTextBox.classList.toggle('editBox'); // меняем класс поля ввода для создания рамки
// };


const newFunc = (parentId, buttonId, noteClass, thisButtonId) => {
    document.querySelector(thisButtonId)
    const parent = document.querySelector(parentId)
    const note = createField(parent, noteClass)
    const button = createButton(parent, buttonId)
    added = true
    newButtonAction(button, note)
    console.log(parent.id)
}

const newButtonAction = (button, note) => {
    const newButton = document.getElementById(button.id)
    let flag = true
    newButton.addEventListener("click", () => {
        if (flag === true) {
            button.textContent = "Сохранить"
            note.contentEditable = true
            note.focus()
            flag = false
        } else {
            button.textContent = "Редактировать"
            note.contentEditable = false
            flag = true
        }
    })
    return true
}

const createField = (parent, className) => {
    const newP = document.createElement('div')
    newP.className = className
    newP.innerHTML = '<h3>Заголовок</h3><br>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
    parent.appendChild(newP);
    return newP
}

let counter = 0;

const createButton = (parent, id, classnameBtn) => {
    counter++;
    const newButton = document.createElement('button')
    newButton.classnameBtn = "classnameBtn"
    newButton.id = id + counter;
    newButton.textContent = 'Редактировать'
    parent.appendChild(newButton);
    return newButton
}